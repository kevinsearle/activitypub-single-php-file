# ActivityPub Server in a Single PHP File

This is a single PHP file - and an `.htaccess file` - which acts as an extremely basic ActivityPub server.

## Getting started

This is designed to be a lightweight educational tool to show you the basics of how ActivityPub works.

There are no tests, no checks, no security features, no header verifications, no containers, no gods, no masters.

Edit the `index.php` file to add a username, password, and keypair.

Upload `index.php` and `.htaccess` to the *root* directory of your domain. For example `test.example.com/`. It will not work in a subdirectory.

Optionally, upload an `icon.png` to make the user look nice.

## How this works

* The `.htaccess` file transforms requests from `example.com/whatever` to `example.com/index.php?path=whatever`.
* The `index.php` file performs a specific action depending on the path requested.
* Log files are saved as .txt in the `/logs` directory.
* Post files are saved as .json in the `/posts` directory.
* This has sloppy support for linking #hashtags, https:// URls, and @ mentions.

##	Requirements

* PHP 8.3 (We live in the future now)
* The [OpenSSL Extension](https://www.php.net/manual/en/book.openssl.php) (This is usually installed by default)
* HTTPS certificate (Let's Encrypt is fine)
* 50MB free disk space (ActivityPub is a very "chatty" protocol. Expect lots of logs.)
* Docker, Node, MongoDB, Wayland, GLaDOS, React, LLM, Adobe Creative Cloud, Maven (Absolutely none of these!)

## Licence

This code is released to you under the [GNU Affero General Public License v3.0 or later](https://www.gnu.org/licenses/agpl-3.0.html).  This means, if you modify this code and let other people interact with it over a computer network, [you **must** release the source code](https://www.gnu.org/licenses/gpl-faq.html#UnreleasedModsAGPL).

I actively do *not* want you to use this code in production. It is not suitable for anything other than educational use.  The use of AGPL is designed to be an incentive for you to learn from this software and then write something better.

Please take note of [CRAPL v0](https://matt.might.net/articles/crapl/):

> Any appearance of design in the Program is purely coincidental and should not in any way be mistaken for evidence of thoughtful software construction.